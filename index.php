<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>LocalStorage</title>
  <link rel="stylesheet" href="output.css">
</head>
<body>
    <header>
        <nav>
           <h1 >KO TO-DO</h1> 
           <div class="button-container">

            <input type="text" class="search navbutton text" placeholder="  Enter Title...">
            <button onclick="validateForm()" class="navbutton">New</button>
            <select class="taskcollection navbutton">
                <option value=""></option>
            </select>
            <button onclick="resetForm()" class="navbutton" >Reset</button>
            <button onclick="deleteForm()" class="navbutton">Delete List</button>
            
            </div>
        </nav>
    </header>

  <div class="wrapper">
    <h2 id="list-header" class="list-name" ></h2>
    <p></p>
    <ul class="list">
      <li>Loading Items...</li>
    </ul>
    <form class="add-items">
      <input type="text" name="item" placeholder="Item Name" required>
      <input type="submit" value="+ Add Item">
    </form>
  </div>

  <script src="main.js"></script>

</body>
</html>

