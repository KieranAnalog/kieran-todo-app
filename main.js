
    const addItems = document.querySelector('.add-items');
    const itemsList = document.querySelector('.list');
    const deleteList = document.querySelectorAll('.delete');

   
    const taskCollection = document.querySelector('.taskcollection');
    populateDropdown(taskCollection);
    try {
        listTitle = taskCollection.options[taskCollection.selectedIndex].value;
    } catch (TypeError) {
        listTitle = 'New List';
    }
    document.getElementById('list-header').innerHTML = listTitle;
    const items = JSON.parse(localStorage.getItem(listTitle)) || [];

    let resetItems=[];

    function validateForm() {
    var newTitle = document.querySelector('.search').value;
    if (newTitle == "") {
        alert("Please select a unique title for your new list");
        return false;
    } else {
         newForm(newTitle);
      }
    }




    function newForm(newTitle){
        localStorage.setItem(newTitle, JSON.stringify(resetItems));
        populateDropdown(itemsList);
        populateListOfItems(resetItems, itemsList);
        updateHeadings(newTitle);
    }





    function resetForm(){
        populateListOfItems(resetItems, itemsList);
        populateDropdown(itemsList);
    }
    function deleteForm() {
        localStorage.removeItem(listTitle);
        populateDropdown(itemsList);
        populateListOfItems(resetItems, itemsList);
    }
    function addItem(e) {
        e.preventDefault();
        const text = (this.querySelector('[name=item]')).value;
        const item = {
            text,
            done: false,
            active: true,
            id: (new Date()).getTime()
        };
        items.push(item);
        populateListOfItems(items, itemsList);
        localStorage.setItem(listTitle, JSON.stringify(items));
        this.reset();
    }

    function populateListOfItems(tasks = [], tasksList) {
        tasksList.innerHTML = tasks.map((tasksli, i) => {
            return `
            <li>
                <input type="checkbox" data-index=${i} id="item${i}" ${tasksli.done ? 'checked' : ''} />
                <label for="item${i}">${tasksli.text}</label>
                <span data-index=${i} id="item${tasksli.id}" class="delete"></span>
            </li>
            
            `;
        }).join('');
    }

    function populateDropdown(tasksList) {
       const keys = Object.keys(localStorage);
        tasksList.innerHTML = keys.map((tasksli, i) => {
            return `
            <option value="${keys[i]}">${keys[i]}</option>
            
            `;
        }).join('');
    }

    function taskControls(e) { 
        // skip this unless it's an input
        if (e.target.matches('input')){
            const el = e.target;
            const index = el.dataset.index;
            items[index].done = !items[index].done;
            localStorage.setItem(listTitle, JSON.stringify(items));
            populateListOfItems(items, itemsList);
        } 
        // if matches the delete button
            else if (e.target.matches('span')) { 
                const el = e.target;
                const index = el.dataset.index;
                items.splice(index, 1);
                localStorage.setItem(itemsList, JSON.stringify(items));
                populateListOfItems(items, itemsList);
        }
    }

    function updateHeadings(newHeadingTitle) {
        document.getElementById('list-header').innerHTML = newHeadingTitle;
    }

    addItems.addEventListener('submit', addItem);
    itemsList.addEventListener('click', taskControls);
    
    taskCollection.addEventListener('change', function() {
        let itemsList = this.options[this.selectedIndex].value;
        console.log(itemsList);
        populateListOfItems(items, itemsList);
        updateHeadings(itemsList);
    });
    populateListOfItems(items, itemsList);